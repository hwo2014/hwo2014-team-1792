import json
import pprint
import socket
import math
import logging

from log import LOG


class GameState(object):
    def __init__(self, my_name, cars, tick):
        self.cars = cars
        self.my_name = my_name
        self.tick = tick
        
    def can_switch_right(self, track, name=None):
        pos = self.get_pos(name)
        lane = pos['lane']['endLaneIndex']
        maxLane = len(track['lanes'])-1
        if lane == maxLane:
            return 0
        
        if 'switch' in self._track_piece(track, pos):
            return self._track_piece(track, pos)['switch']
        else:
            return 0
    
    def can_switch_left(self, track, name=None):
        pos = self.get_pos(name)   
        lane = pos['lane']['endLaneIndex']        
        minLane = 0
        if lane == minLane:
            return 0
        
        if 'switch' in self._track_piece(track, pos):
            return self._track_piece(track, pos)['switch']
        else:
            return 0

    def get_angle(self, name=None):
        return self.get_by_name(name)['angle']

    def get_pos(self, name=None):
        return self.get_by_name(name)['piecePosition']

    def get_speed(self, name=None):
        return self.get_by_name(name)['speed']

    def get_hardness(self, track, name=None):
        pos = self.get_pos(name)
        return self._track_piece(track, pos)['hardness']                
        
    def get_cur_piece_len(self, track, name=None):
        pos = self.get_pos(name)
        lane = pos['lane']['startLaneIndex']
        return self._track_piece(track, pos)['length'][lane]

    def get_cur_piece_angle(self, track, name=None):
        pos = self.get_pos(name)
        if 'angle' in self._track_piece(track, pos):
            return self._track_piece(track, pos)['angle']
        else:
            return 0.0

    def get_cur_piece_radius(self, track, name=None):
        pos = self.get_pos(name)
        if 'angle' in self._track_piece(track, pos):
            return self._track_piece(track, pos)['radius']
        else:
            return 0.0

    def get_next_curve(self, track, name=None):
        """Returns (next_curve_piece, distance_to_next_curve)
        """

        pos = self.get_pos(name)
        lane = pos['lane']['endLaneIndex']
        if 'angle' in self._track_piece(track, pos):
            return track['pieces'][pos['pieceIndex']], 0.0
        next_idx = pos['pieceIndex']
        while True:
            next_idx = self.get_next_piece_index(next_idx, track)
            if 'angle'in track['pieces'][next_idx]:
                return (track['pieces'][next_idx],
                        self.piece_distance(pos['pieceIndex'],
                                            next_idx, track, lane)
                        + self._track_piece(track, pos)['length'][lane]
                        - pos['inPieceDistance'] - track['pieces'][next_idx]['length'][lane])

    def get_next_piece_index(self, piece_idx, track):
        idx = piece_idx + 1
        if idx >= len(track['pieces']):
            return 0
        else:
            return idx

    def get_prev_piece_index(self, piece_idx, track):
        idx = piece_idx - 1
        if idx < 0:
            return len(track['pieces']) - 1
        else:
            return idx

    def _track_piece(self, track, pos):
        return track['pieces'][pos['pieceIndex']]

    def piece_distance(self, a_piece, b_piece, track, lane=0):
        c_piece = b_piece
        d = 0
        while a_piece != c_piece:
            d += track['pieces'][c_piece]['length'][lane]
            c_piece = self.get_prev_piece_index(c_piece, track)
        return d

    def pos_distane(self, a_pos, b_pos, track):
        last_lane = a_pos['lane']['startLaneIndex']
        curr_lane = b_pos['lane']['startLaneIndex']

        delta_s = self.piece_distance(a_pos['pieceIndex'], b_pos['pieceIndex'], track, last_lane)
        delta_s += (self._track_piece(track, a_pos)['length'][last_lane] - a_pos['inPieceDistance'])
        delta_s -= (self._track_piece(track, b_pos)['length'][curr_lane] - b_pos['inPieceDistance'])
        return delta_s

    def update_speed(self, last_state, track):
        delta_t = self.tick - last_state.tick
        for car in self.cars:
            last_pos = last_state.get_pos(car['id']['name'])
            curr_pos = self.get_pos(car['id']['name'])
            delta_s = self.pos_distane(last_pos, curr_pos, track)
            if delta_t < 0.0:
                # we spawned after a crash
                car['speed'] = 0.0
            elif curr_pos['pieceIndex'] + 1 == last_pos['pieceIndex']:
                # we spawned after a crash
                # back to the prev track piece
                car['speed'] = 0.0
            elif (curr_pos['pieceIndex'] == len(track['pieces'])
                  and last_pos['pieceIndex'] == 0):
                # we spawned after a crash
                # and put back before the finish line
                car['speed'] = 0.0
            elif delta_t == 0:
                car['speed'] = 0.0
            else:
                car['speed'] = delta_s / delta_t

    def get_by_name(self, name):
        if not name:
            name = self.my_name

        for car in self.cars:
            if car['id']['name'] == name:
                return car
        raise ValueError("No car is in game with name %s " % name)


class BotBase(object):
    def __init__(self, socket, name, key, race, is_host, send_tick=False):
        self.socket = socket
        self.name = name
        self.key = key

        if race:
            self.race = {"botId": {"name": self.name,
                                   "key": self.key}}
            self.race.update(race)
        else:
            self.race = None

        self.is_host = is_host
        self.send_tick = send_tick
        self.tick = 0
        self.state = None
        self.last_throttle = 0.0
        self.track = None

        FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
        file = logging.FileHandler(filename=self.name + '.log', mode='w')
        file.setLevel(logging.DEBUG)
        file.setFormatter(logging.Formatter(FORMAT))
        LOG.addHandler(file)

    def msg(self, msg_type, data):
        m = {"msgType": msg_type, "data": data}
        if self.send_tick:
            m['gameTick'] = self.tick
        LOG.debug("%s MSG sent\n%s" % (self.name, pprint.pformat(m)))
        self.send(json.dumps(m))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})
        
    def switch_right(self):
        self.msg("switchLane", "Right")     
    
    def switch_left(self):
        self.msg("switchLane", "Left")    

    def create_race(self):
        self.msg("createRace", self.race)

    def join_race(self):
        self.msg("joinRace", self.race)

    def run(self):
        if self.race:
            if self.is_host:
                self.create_race()
            else:
                self.join_race()
        else:
            #we have to keep the
            #original join protocol
            #as it will be used in CI
            self.join()

        self.msg_loop()

    def on_join(self, data):
        LOG.info("%s Joined" % self.name)
        self.ping()

    def on_game_start(self, data):
        LOG.info("%s Race started" % self.name)
        self.ping()

    def on_crash(self, data):
        if data["name"] == self.name:
            LOG.info("%s Crashed. %s"
                     % (self.name,
                        self.get_telemetry(self.state,
                                           self.last_throttle)))
        self.ping()

    def on_game_end(self, data):
        LOG.info("%s Race ended" % self.name)
        self.ping()

    def on_error(self, data):
        LOG.error("%s Error: %s" % (self.name, data))
        self.ping()

    def on_unhandled(self, msg_type, data):
        LOG.warning("%s Unhandled MSG type %s" % (self.name, msg_type))
        self.ping()

    def on_lap_finished(self, data):
        lap_time = data['lapTime']
        if data['car']['name'] == self.name:
            LOG.info("%s Lap finished %d ms (%d ticks)" % (self.name,
                                                           lap_time['millis'],
                                                           lap_time['ticks']))

    def on_create_race(self, data):
        self.ping()

    def on_game_init(self, data):
        self.track = data['race']['track']
        self.calculate_curve_hardness(self.track)
        self.calculate_curve_length(self.track)

    def calculate_curve_length(self, track):
        for piece in track['pieces']:
            if 'angle' in piece:
                piece['length'] = []
                for lane in track['lanes']:
                    radius = piece['radius'] + -1 * cmp(piece['angle'], 0) * lane['distanceFromCenter']
                    length = 2 * radius * math.pi * abs(piece['angle']) / 360
                    piece['length'].append(length)
            else:
                # duplicate length for every lane
                length = piece['length']
                piece['length'] = [length] * len(track['lanes'])

    def calculate_curve_hardness(self, track):
        for piece in track['pieces']:
            if 'angle' in piece:
                piece['hardness'] = ((abs(piece['angle']) / piece['radius']) / 360) * 100
            else:
                piece['hardness'] = 0.0
            if piece['hardness'] > 1.0:
                return 1.0

    def do_nothing(self, data):
        pass            

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
            'createRace': self.on_create_race,
            'gameInit': self.on_game_init,
            'spawn': self.do_nothing,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            LOG.debug("%s MSG received\n%s" % (self.name, pprint.pformat(msg)))
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.on_unhandled(msg_type, data)
            line = socket_file.readline()
        LOG.info("%s Connection closed" % self.name)

    def prepare_game_state(self, data):
        new_state = GameState(self.name, data, self.tick)
        if not self.state:
            #first tick
            self.state = new_state
        new_state.update_speed(self.state, self.track)
        return new_state

    def on_car_positions(self, data):
        new_state = self.prepare_game_state(data)

        throttle = self.ai_get_throttle(self.state, new_state)

        if throttle > 1.0:
            throttle = 1.0
        if throttle < 0.0:
            throttle = 0.0

        self.state = new_state
        self.last_throttle = throttle
        LOG.debug("%s TEM %s" % (self.name, self.get_telemetry(new_state, throttle)))    
            
        self.throttle(throttle)

    def get_telemetry(self, new_state, new_throttle):
        tem = ("tick=%d angle=%f speed=%f throttle=%f hardness=%f piece=%d "
               "in_piece=%f length=%f piece_angle=%f piece_radius=%f" %
               (self.tick, new_state.get_angle(), new_state.get_speed(),
                new_throttle, new_state.get_hardness(self.track),
                new_state.get_pos()['pieceIndex'], new_state.get_pos()['inPieceDistance'],
                new_state.get_cur_piece_len(self.track),
                new_state.get_cur_piece_angle(self.track),
                new_state.get_cur_piece_radius(self.track)))
        return tem



    def ai_get_throttle(self, old_state, new_state):
        raise NotImplementedError()


def start_bot(host, port, name, key, bot_class, race, is_host):
    print("Connecting with parameters:")
    print("host=%s, port=%s, bot name=%s, key=%s" % (host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    LOG.info("%s Connecting..." % name)
    s.connect((host, int(port)))
    LOG.info("%s Connected" % name)
    bot = bot_class(s, name, key, race, is_host)
    bot.run()
