from wsgiref.util import is_hop_by_hop
import bots
import game
import multiprocessing
import sys
import time

BOTS = [
#    (bots.NoobBot, "GTG-noob"),
    (bots.ConservativeBreaker, "GTG-consbreak"),
]

RACE = {"trackName": "usa",
        "password": "pokmalac",
        "carCount": len(BOTS)}


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python multi_race.py host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]

        procs = []

        race_hosting_bot = BOTS[0]
        p = multiprocessing.Process(target=game.start_bot,
                                    args=[host, port, race_hosting_bot[1], key,
                                          race_hosting_bot[0], RACE, True])
        procs.append((race_hosting_bot[1], p))
        p.start()
        #HACK: allow time to get the race registered
        #before starting other bots
        time.sleep(2)

        for bot_clazz, name in BOTS[1:]:
            p = multiprocessing.Process(target=game.start_bot,
                                        args=[host, port, name, key,
                                              bot_clazz, RACE, False])
            procs.append((name, p))
            p.start()

        for name, p in procs:
            p.join()
            print "Process for %s exited" % name
