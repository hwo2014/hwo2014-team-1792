from log import LOG
from game import GameState
from game import BotBase


class NoobBot(BotBase):
    def __init__(self, socket, name, key, race, is_host):
        super(NoobBot, self).__init__(socket, name, key, race, is_host, send_tick=True)

    def ai_get_throttle(self, old_state, new_state):
        my_last_angle = old_state.get_angle()
        my_angle = new_state.get_angle()
        throttle = self.last_throttle

        if 0.0 < abs(my_last_angle) < abs(my_angle):
            throttle /= (abs(my_angle) / abs(my_last_angle)) * 0.99
        else:
            throttle = 1.0

        return throttle


class ConservativeBreaker(BotBase):
    def __init__(self, socket, name, key, race, is_host):
        super(ConservativeBreaker, self).__init__(socket, name, key, race, is_host, send_tick=True)

    def ai_get_throttle(self, old_state, new_state):
        my_last_angle = old_state.get_angle()
        my_angle = new_state.get_angle()
        throttle = self.last_throttle
        next_curve_piece, next_curve_distance = new_state.get_next_curve(self.track)
        max_speed = (4.80 + (1 / next_curve_piece['hardness']) * (next_curve_distance / 100))
        if (new_state.get_speed() > max_speed
                or abs(new_state.get_angle()) > 55.0):
            throttle = 0.0
        else:
            throttle = 1.0
        tem = self.get_telemetry(new_state, throttle)
        tem = "max_speed=%f next_c_dist=%f " % (max_speed, next_curve_distance) + tem
        LOG.debug("%s TEM2 %s" % (self.name, tem))
        return throttle
        
class LaneSwitching(BotBase):
    def __init__(self, socket, name, key, race, is_host):
        super(LaneSwitching, self).__init__(socket, name, key, race, is_host, send_tick=True)
        
    def ai_get_throttle(self, old_state, new_state):
        my_last_angle = old_state.get_angle()
        my_angle = new_state.get_angle()
        throttle = self.last_throttle
        next_curve_piece, next_curve_distance = new_state.get_next_curve(self.track)
        max_speed = (5.20 + (1 / next_curve_piece['hardness']) * (next_curve_distance / 100))
        if (new_state.get_speed() > max_speed
                or abs(new_state.get_angle()) > 55.0):
            throttle = 0.0
        else:
            throttle = 1.0
        tem = self.get_telemetry(new_state, throttle)
        tem = "max_speed=%f next_c_dist=%f " % (max_speed, next_curve_distance) + tem
        LOG.debug("%s TEM2 %s" % (self.name, tem))
        return throttle
    
    def ai_should_switch_right(self, new_state):
        next_curve_piece, next_curve_distance = new_state.get_next_curve(self.track)
        if (next_curve_piece['angle'] > 0):
            return 1;
        return 0;
        
    def ai_should_switch_left(self, new_state):
        next_curve_piece, next_curve_distance = new_state.get_next_curve(self.track)
        if (next_curve_piece['angle'] < 0):
            return 1;
        return 0;
    
    def on_car_positions(self, data):
        new_state = self.prepare_game_state(data)

        throttle = self.ai_get_throttle(self.state, new_state)

        if throttle > 1.0:
            throttle = 1.0
        if throttle < 0.0:
            throttle = 0.0

        self.state = new_state
        self.last_throttle = throttle
        LOG.debug("%s TEM %s" % (self.name, self.get_telemetry(new_state, throttle)))

        next_curve_piece, next_curve_distance = new_state.get_next_curve(self.track)
        
        if (self.ai_should_switch_right(new_state)):
            if (next_curve_distance < 45 and abs(next_curve_piece['angle']) > 45):
                throttle = 0.0                
                self.last_throttle = throttle
                
            if (new_state.can_switch_right(self.track)):
                LOG.info("Switching right")
                return self.switch_right();      

        if (self.ai_should_switch_left(new_state)):
            if (next_curve_distance < 45 and abs(next_curve_piece['angle']) > 45):
                throttle = 0.0
                self.last_throttle = throttle
                
            if (new_state.can_switch_left(self.track)):
                LOG.info("Switching left")
                return self.switch_left();                   
            
        self.throttle(throttle)



