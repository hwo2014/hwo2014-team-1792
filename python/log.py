import logging
import sys

LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)

stdout = logging.StreamHandler(stream=sys.stdout)
stdout.setLevel(logging.INFO)

LOG.addHandler(stdout)

