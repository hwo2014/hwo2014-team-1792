import sys

import bots
import game

KEIMOLA = {"trackName": "keimola",
        "password": "pokmalac",
        "carCount": 1}
        
GERMANY = {"trackName": "germany",
        "password": "pokmalac",
        "carCount": 1}
        
USA = {"trackName": "usa",
        "password": "pokmalac",
        "carCount": 1}

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        game.start_bot(host, port, name, key, bots.LaneSwitching, KEIMOLA, None)
